document.getElementById("countButton").onclick = function () {
    let typedText = document.getElementById("textInput").value;
    typedText = typedText.toLowerCase();
    typedText = typedText.replace(/[^a-z'\s]+/g, "");
    const letras = typedText.split('');
    const palavras = typedText.split(' ');
    console.log(palavras);
    const letterCounts = {};
    const wordCounts = {};
    for(let i=0;i<letras.length;i++){
        currentLetter = letras[i];
        if (letterCounts[currentLetter] === undefined) {
            letterCounts[currentLetter] = 1; 
         } else { 
            letterCounts[currentLetter]++; 
         }
    }
    for (let letter in letterCounts) { 
        const span = document.createElement("span"); 
        const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", "); 
        span.appendChild(textContent); 
        document.getElementById("lettersDiv").appendChild(span); 
     }

     for(let i=0;i<palavras.length;i++){
        currentword = palavras[i];
        if (wordCounts[currentword] === undefined) {
            wordCounts[currentword] = 1; 
         } else { 
            wordCounts[currentword]++; 
         }
    }
    for (let word in wordCounts) { 
        const span = document.createElement("span"); 
        const textContent = document.createTextNode('"' + word + "\": " + wordCounts[word] + ", "); 
        span.appendChild(textContent); 
        document.getElementById("wordsDiv").appendChild(span); 
     }
    console.log(letterCounts);
}